from django.contrib import admin
from .models import Tour, Comment, Question, Choice, UserDetails, Gallery, AboutUsPictures

admin.site.register(Tour)
admin.site.register(Comment)
admin.site.register(UserDetails)


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'photo')

admin.site.register(Gallery, GalleryAdmin)
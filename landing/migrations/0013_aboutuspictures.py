# Generated by Django 3.0.3 on 2020-06-24 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0012_tour_on_click_bookatour'),
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUsPictures',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('photo', models.ImageField(upload_to='')),
            ],
        ),
    ]

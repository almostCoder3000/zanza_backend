from django.db import models
from django_resized import ResizedImageField
from sorl.thumbnail import ImageField, get_thumbnail


PAGE_TYPES = (
    ('main', 'Main Page'),
    ('au', 'Australia Page'),
    ('nz', 'New-Zealand Page'),
)

class AboutUsPictures(models.Model):
    title = models.CharField(max_length=300)
    photo = models.ImageField(upload_to='')

    def __str__(self):
        return self.title


class Tour(models.Model):
    title = models.CharField(max_length=300)
    price = models.IntegerField()
    description = models.TextField(blank=True)
    photo = ResizedImageField(upload_to='', blank=True, size=[300, 160])
    title_hebrew = models.CharField(max_length=300, default="")
    description_hebrew = models.TextField(blank=True, default="")
    on_click_bookatour = models.CharField(max_length=500, blank=True, null=True)
    type_page = models.CharField(
                    choices=PAGE_TYPES, max_length=50, 
                    default="main", verbose_name=u"Type of page")

    def __str__(self):
        return self.title
    

class Comment(models.Model):
    title = models.CharField(max_length=100, default="Title")
    text = models.TextField(blank=True)
    date = models.DateField(auto_now_add=True)
    addressant = models.CharField(max_length=100)
    rating = models.IntegerField(default=5)
    is_main_page = models.BooleanField(default=True, verbose_name="For main page")

    def __str__(self):
        return self.addressant
    


class Question(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text
    


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    isTruth = models.BooleanField(default=False)

    def __str__(self):
        return self.choice_text
    

class UserDetails(models.Model):
    name = models.CharField(max_length=300, blank=True)
    phone_number = models.CharField(max_length=300, blank=True)
    email = models.CharField(max_length=300)
    isAgree = models.BooleanField(default=False)
    test_result = models.TextField(blank=True)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = u"Contact"
        verbose_name_plural = u"Contacts"
    
class Gallery(models.Model):
    title = models.CharField(max_length=300)
    photo = ImageField(upload_to='', blank=True)
    preview = models.ImageField(upload_to='', blank=True, editable=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.photo:
            self.preview = get_thumbnail(self.photo, '500x600').url
        super(Gallery, self).save(*args, **kwargs)